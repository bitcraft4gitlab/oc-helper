<?php namespace Bitcraft\Helper\Behaviors;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManager;
use RainLab\Translate\Models\Locale;

class ResizeImagesBehavior extends \October\Rain\Extension\ExtensionBase
{
    protected $parent;
    protected $disk;

    public function __construct($parent)
    {
        $this->parent = $parent;
        $this->disk = $this->parent->resize_storage ?? 's3';
    }

    public function resizeAfterSave()
    {
        $locales = array_keys(Locale::listAvailable());
        foreach ($locales as $locale) {
            foreach ($this->parent->getAttributeTranslated('modules', $locale) as $module) {
                foreach ($module as $index => $value) {
                    if (is_array($value)) {
                        foreach ($value as $row) {
                            switch ($row['_group']) {
                                case 'image_list_module':
                                    foreach ($row['images'] as $image) {
                                        $this->handleImageSizes($image);
                                    }
                                    break;
                                case 'image_module':
                                    $this->handleImageSizes($row);
                                    break;
                            }
                        }
                    }
                }
            }
        }
    }

    public function handleImageSizes($item)
    {
        $valid_mime_types = ['.png', '.jpg', '.jpeg'];
        $meta = explode('.', $item['image']);
        $extension = last($meta);
        if (array_key_exists('image_size', $item)
            && Storage::disk($this->disk)->exists("media{$item['image']}")
            && in_array('.'.$extension, $valid_mime_types, true)) {
            // to webp
            $image = Storage::disk($this->disk)->get("media{$item['image']}");
            $webp_path = str_replace($valid_mime_types, '.webp', $item['image']);
            if (!Storage::disk($this->disk)->exists("media$webp_path")) {
                $manager = new ImageManager(['driver' => 'gd']);
                $webp_img = $manager->make($image);
                $webp_img = $webp_img->encode('webp');
                Storage::disk($this->disk)->put("media$webp_path", $webp_img->__toString());
            }

            // to size
            if ($image_size = $item['image_size']) {
                $this->saveImageWithSize(
                    $image_size,
                    "media{$item['image']}",
                    "media$webp_path",
                    'webp'
                );

                $this->saveImageWithSize(
                    $image_size,
                    "media{$item['image']}",
                    "media{$item['image']}",
                    $extension
                );
            }
        }
    }

    public function saveImageWithSize($image_size, $image_path, $to, $encode)
    {
        if (preg_match('/(\d+|auto)x(\d+|auto)/', $image_size, $match)) {
            $width = $match[1] === 'auto' ? null : $match[1];
            $height = $match[2] === 'auto' ? null : $match[2];

            if (Storage::disk($this->disk)->exists($image_path)) {
                $image = Storage::disk($this->disk)->get($image_path);
                $img_size_path = str_replace('media', "media/sizes/$image_size", $to);

                if (Storage::disk($this->disk)->exists($img_size_path)) {
                    return;
                }

                $manager = new ImageManager(['driver' => 'gd']);
                $img_with_size = $manager->make($image);
                $img_with_size = $width === null || $height === null
                    ? $img_with_size->resize($width, $height, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    : $img_with_size->crop($width, $height);
                $img_with_size = $img_with_size->encode($encode);
                Storage::disk($this->disk)->put($img_size_path, $img_with_size->__toString());
            }
        }
    }

}
